import { Component, OnInit } from '@angular/core';
import { timeout } from 'q';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  /*
   * Global variables used for Servers component
   */
  allowNewServer = false;
  newServerCreated = false;
  serverName = '';

  /*
   * Constructor method, things that are done on onCreate()
   */
  constructor() {
    setTimeout(() => this.allowNewServer = true, 2000);
    // Arrow function is used because this can be used outside the scope of the function.
   }

  onCreateNewServer() {
    this.newServerCreated = true;
  }

  /**
   * Updates a global variable named serverName.
   * The variable is updated using a html input element that is controller by a user.
   *
   * @param event Event being listen for.
   */
  onUpdateServerName(event: Event) {
    // HTMLInputElemt is need to cast the type correctly as typescript does
    // not like the default type provided.
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  ngOnInit() {
  }

}
