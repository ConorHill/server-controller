import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  /**
   * Global variables used
   */
  serverId = 10;
  serverStatus: string;

  constructor() {
    this.serverStatus = Math.random() < 0.5 ? 'Online' : 'Offline';
   }

  ngOnInit() {
  }

  getServerStatus() {
    return this.serverStatus;
  }

  getColor() {
    return this.serverStatus === 'Offline' ? 'red' : 'green';
  }
}
